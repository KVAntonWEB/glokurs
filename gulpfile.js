/*jshint esversion: 8 */
const gulp = require("gulp"), //gulp
    gutil = require("gulp-util"), //вспомогательные утилиты gulp
    postcss = require("gulp-postcss"), //postCSS
    assets = require("postcss-assets"), // инлайн картинок, cachebuster, размер inline(''), size('')
    autoprefixer = require("autoprefixer"),
    cssnano = require("cssnano"),
    purgecss = require("@fullhuman/postcss-purgecss"),
    // uglify = require('gulp-uglify-es').default, //минификация JS
    uglify = require("gulp-terser"), //минификация JS
    htmlmin = require("gulp-htmlmin"), //минификация html
    filesize = require("gulp-size"), //размер файла
    rename = require("gulp-rename"), //переименование (минифицированные файлы)
    concat = require("gulp-concat"), //соединение файлов
    sass = require("gulp-sass"), //компиляция sass
    imagemin = require("gulp-imagemin"), //оптимизация изображений
    webp = require("gulp-webp"),
    cache = require("gulp-cache"), // Подключаем библиотеку кеширования
    browser_sync = require("browser-sync"), //LiveReload
    spritesmith = require("gulp.spritesmith"), //Спрайты
    sourcemaps = require("gulp-sourcemaps"), //Создание кар исходников JS (для читаемости минифицированных файлов)
    babel = require("gulp-babel"), //JS es6 -> ex5
    rigger = require("gulp-rigger"), //Вставление одного файла в другой
    svgSprite = require("gulp-svg-sprites"), // SVG спрайты
    cheerio = require("gulp-cheerio"), //JQuery для сервера (обработка данных)
    imgsizefix = require("gulp-imgsizefix"), // добавление тегу img - атрибутов heigh/weight
    del = require("del"),
    nunjucks = require("gulp-nunjucks-render"),
    prettier = require("gulp-prettier"),
    rev = require("gulp-rev"),
    revReplace = require("gulp-rev-replace");

const debug = require("gulp-debug");
var path_project = ".";
var path_localhost = "./app/";

var paths_res = {
    images: [
        path_project + "/resources/img/**/*",
        path_project + "/resources/img/*"
    ],
    sprites: {
        root: path_project + "/resources/sprites/",
        png: path_project + "/resources/sprites/png/*.png",
        jpg: path_project + "/resources/sprites/jpg/*.jpg",
        svg: path_project + "/resources/sprites/svg/*.svg"
    },
    svg: path_project + "/resources/svg/*.svg",
    fonts: path_project + "/resources/fonts/**/*",
    favicon: path_project + "/resources/favicon/*"
};

var paths_localhost = {
    root: path_localhost,
    assets: path_localhost + "assets/"
};

let manifest_filename = "rev-manifest.json";
var paths_assets = {
    all: paths_localhost.assets + "*",
    manifest: paths_localhost.assets + manifest_filename,
    assets: [paths_localhost.assets + "**/*"],
    templates: paths_localhost.root,
    templates_html: paths_localhost.root,
    css: paths_localhost.assets + "css/",
    js: paths_localhost.assets + "js/",
    vendor: paths_localhost.assets + "vendor/",
    chunks: paths_localhost.assets + "chunks/",
    img: paths_localhost.assets + "img/",
    favicon: paths_localhost.assets + "favicon/",
    sprites: {
        root: paths_localhost.assets + "sprites/",
        png: paths_localhost.assets + "sprites/png/",
        jpg: paths_localhost.assets + "sprites/jpg/",
        svg: paths_localhost.assets + "sprites/svg/"
    },
    data: paths_localhost + "data/",
    fonts: paths_localhost.assets + "fonts/"
};

var paths_src = {
    root: "./src/",
    data: ["./src/data/*.html", "./src/data/**/*.html"],
    common_project_sass: [
        "./src/common.project/**/*.scss",
        "./src/common.project/*.scss"
    ],
    sass: [
        "./src/common.project/init.project.scss",
        "./src/common.project/slick/*.css",
        "./src/sprites/svg/_svg_sprite.scss",
        "./src/template/*.scss",
        "./src/template/**/*.scss",
        "./src/sections/**/*.scss",
        "!./src/template/**/__*.scss",
        "./src/chunks/**/*.scss",
        "!./src/chunks/**/__*.scss"
    ],
    sass_mv: ["./src/template/**/__*.scss", "./src/chunks/**/__*.scss"],
    templates: [
        "./src/template/*.html",
        "./src/template/**/*.html",
        "!./src/template/~*"
    ],
    chunks: [
        "./src/chunks/**/*.html",
        "./src/sections/**/*.html",
        "!./src/chunks/**/~*"
    ],
    chunks_mv: ["./src/chunks/**/~*.html", "./src/chunks/~*.html"],
    vendor: "./src/vendor/*.js",
    script: [
        "./node_modules/vanilla-lazyload/dist/lazyload.min.js",
        "./node_modules/tilt.js/dest/tilt.jquery.min.js",
        "./src/js/*.js",
        "./src/sections/**/*.js",
        "./src/chunks/**/*.js",
        "!./src/js/_*.js"
    ],
    script_mv: ["./src/js/__*.js*"],
    script_min: ["./src/js/_[A-Z,a-z]*.js"],
    sprites: {
        svg_template: "./src/sprites/svg/_sprite-template.scss",
        svg: "./src/sprites/svg/_svg_sprite.scss",
        png: "./src/sprites/png/",
        jpg: "./src/sprites/jpg/"
    }
};

//===========================================
//================= Tools ===================
//===========================================

function browserSync(done) {
    browser_sync({
        browser: "chromium-browser",
        // proxy: "glokurs.local",
        server: {
            baseDir: path_localhost
        }
    });
    done();
}

function browserSyncReload(done) {
    browser_sync.reload();
    done();
}

//===========================================
//============ Item Template ================
//===========================================

function item_clean(item_path) {
    return del(item_path, { force: true });
}

function item_mv(item_src, item_dest) {
    return gulp.src(item_src).pipe(gulp.dest(item_dest));
}

async function item_watch(item_path, item_task) {
    gulp.watch(item_path, gulp.series(item_task, browserSyncReload));
}

function item_html_build(item_src, item_dest, item_name) {
    return (
        gulp
            .src(item_src) //Выберем файлы по нужному пути
            .pipe(rigger()) //Прогоним через rigger
            .pipe(nunjucks())
            .on("error", function(err) {
                gutil.log(gutil.colors.red("[Error]"), err.toString());
            })
            .pipe(revReplace({ manifest: gulp.src("./" + manifest_filename) }))
            .on("error", gutil.log)
            .pipe(prettier())
            .pipe(gulp.dest(item_dest)) //Выплюнем их в папку build
            // .pipe(filesize({ title: item_name + ' full' }))
            .pipe(
                cache(
                    htmlmin({
                        minifyJS: true,
                        collapseWhitespace: true,
                        removeComments: true,
                        removeScriptTypeAttributes: true,
                        removeStyleLinkTypeAttributes: true,
                        sortAttributes: true,
                        ignoreCustomFragments: [/{[/S/s]*}/]
                    })
                )
            )
            .pipe(rename({ suffix: ".min" }))
            .pipe(gulp.dest(item_dest))
            // .pipe(filesize({ title: item_name + ' min' }))
            // .pipe(filesize({ title: item_name + ' min gzip', gzip: true }))
            .on("error", gutil.log)
    );
}

//===========================================
//=========== HTML Templates ================
//===========================================

function templates_clean() {
    return item_clean(paths_assets.templates_html);
}

function templates_build() {
    return item_html_build(
        paths_src.templates,
        paths_assets.templates_html,
        "HTML"
    );
}

async function data_watch() {
    item_watch(paths_src.data, "html:build");
}
async function templates_watch() {
    item_watch(paths_src.templates, "html:build");
}

//===========================================
//============= Chunks ======================
//===========================================

function chunks_clean() {
    return item_clean(paths_assets.chunks);
}

function chunks_build() {
    return item_html_build(paths_src.chunks, paths_assets.chunks, "Chunks");
}

function chunks_fenom_build() {
    return item_mv(paths_src.chunks_mv, paths_assets.chunks, "HTML");
}

async function chunks_watch() {
    item_watch(paths_src.chunks, "html:build");
}

//===========================================
//=============== SASS ======================
//===========================================

function sass_clean() {
    return item_clean([paths_assets.css + "/*"]);
}

function sass_mv() {
    return gulp
        .src(paths_src.sass_mv)
        .pipe(concat("style_second.scss"))
        .pipe(sass().on("error", sass.logError))
        .pipe(
            cache(
                postcss(
                    [
                        require("postcss-focus"),
                        require("postcss-pxtorem"),
                        assets({
                            loadPaths: [
                                paths_assets.img,
                                paths_assets.fonts,
                                paths_assets.sprites.root
                            ],
                            cachebuster: true,
                            cache: true
                        }),
                        autoprefixer(),
                        cssnano()
                    ],
                    { syntax: require("postcss-scss") }
                )
            )
        )
        .pipe(rename({ suffix: ".min", extname: ".css" }))
        .pipe(filesize({ title: "sass min" }))
        .pipe(filesize({ title: "sass min gzip", gzip: true }))
        .pipe(gulp.dest(paths_assets.css));
}

function sass_build() {
    return (
        gulp
            .src(paths_src.sass)
            // .pipe(sourcemaps.init({ loadMaps: true }))
            // .pipe(rigger()).on('error', gutil.log)
            .pipe(concat("style.scss"))
            .pipe(sass().on("error", sass.logError))
            .pipe(filesize({ title: "sass full" }))
            .pipe(
                postcss(
                    [
                        require("postcss-focus"),
                        require("postcss-pxtorem"),
                        require("postcss-url")({
                            url: "inline",
                            basePath: [
                                path_project,
                                `${path_project}src/`,
                                `${path_project}src/css/`,
                                `${path_project}resources/img/`
                            ]
                        }),
                        assets({
                            loadPaths: [
                                paths_assets.img,
                                paths_assets.fonts,
                                paths_assets.sprites.root
                            ],
                            cachebuster: true,
                            cache: true
                        }),
                        require("postcss-preset-env")({
                            browsers: "last 10 versions",
                            stage: 3,
                            features: {
                                "nesting-rules": true
                            }
                        }),
                        purgecss({
                            content: [
                                ...paths_src.script,
                                ...paths_src.templates,
                                ...paths_src.chunks
                            ],
                            keyframes: true,
                            variables: true,
                            whitelist: [
                                "flex-control-nav",
                                "flex-direction-nav",
                                "slides",
                                "flexslider",
                                "flex-direction-nav",
                                "flex-control-paging",
                                '[class*=" icon-"]',
                                "[class^=icon-]",
                                "#gradient-vertical"
                            ],
                            whitelistPatterns: [
                                /^lightbox.*$/,
                                /.*gradient-.*$/,
                                /^slick.*$/,
                                /^icon.*$/,
                                /.*ico.*$/,
                                /.*ico.*/,
                                /^icon-svg.*$/,
                                /^icomoon.*$/,
                                /^kw-.*/
                            ],
                            whitelistPatternsChildren: [
                                /^icon-svg.*$/,
                                /^kw-.*/
                            ]
                        }),
                        autoprefixer(),
                            
                        cssnano()
                    ],
                    { syntax: require("postcss-scss") }
                )
            )
            // .pipe(sourcemaps.write())
            .pipe(rename({ suffix: ".min", extname: ".css" }))
            .pipe(filesize({ title: "sass min" }))
            .pipe(filesize({ title: "sass min gzip", gzip: true }))
            .pipe(gulp.dest(paths_assets.css))
    );
}

async function sass_watch() {
    item_watch(paths_src.sass, gulp.series("html:build"));
    item_watch(paths_src.common_project_sass, gulp.series("html:build"));
}

//===========================================
//=============== JS ========================
//===========================================

function js_clean() {
    return item_clean(paths_assets.js);
}

function js_build() {
    return (
        gulp
            .src(paths_src.script)
            .pipe(rigger())
            // .pipe(sourcemaps.init({ loadMaps: true }))
            .pipe(
                    babel({
                        presets: ["@babel/preset-env"]
                    })
            )
            .pipe(concat("site.js"))
            .pipe(prettier())
            .pipe(gulp.dest(paths_assets.js))
            .pipe(
                    uglify()
            )
            .on("error", function(err) {
                console.log(err.toString());
                this.emit("end");
            })
            // .pipe(sourcemaps.write())
            .pipe(rename({ suffix: ".min" }))
            .pipe(gulp.dest(paths_assets.js))
            .pipe(filesize({ title: "scripts min" }))
            .pipe(filesize({ title: "scripts min gzip", gzip: true }))
            .on("error", gutil.log)
    );
}

function js_mv(done) {
    item_mv(paths_src.script_mv, paths_assets.js);
    done();
}

function js_min(done) {
    return gulp
        .src(paths_src.script_min)
        .pipe(rigger())
        .pipe(gulp.dest(paths_assets.js))
        .pipe(
            cache(
                uglify({
                    keep_classnames: true,
                    keep_fnames: true,
                    safari10: true
                })
            )
        )
        .on("error", function(err) {
            console.log(err.toString());
            // gutil.log(gutil.colors.red('[Error]'), err.toString());
            this.emit("end");
        })
        .pipe(rename({ suffix: ".min" }))
        .pipe(gulp.dest(paths_assets.js))
        .on("error", gutil.log);
}

async function js_watch() {
    item_watch(
        paths_src.script.concat(paths_src.script_min),
        gulp.series("html:build")
    );
}

//===========================================
//============== Sprite =====================
//===========================================

function sprite_jpg_clean() {
    return item_clean(paths_assets.sprites.jpg);
}

function sprite_png_clean() {
    return item_clean(paths_assets.sprites.png);
}

function sprite_svg_clean() {
    return item_clean(paths_assets.sprites.svg);
}

function sprite_png(done) {
    var spriteData = gulp
        .src(paths_res.sprites.png)
        .pipe(cache(imagemin([imagemin.optipng({ optimizationLevel: 7 })])))
        .pipe(
            spritesmith({
                imgName: "sprite_png.png",
                cssName: "sprite_png.scss",
                imgPath: "/assets/sprites/png/sprite_png.png",
                padding: 5,
                algorithm: "binary-tree"
            })
        );

    spriteData.img.pipe(gulp.dest(paths_assets.sprites.png)); // путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest(paths_src.sprites.png)); // путь, куда сохраняем стили
    done();
}

function sprite_jpg(done) {
    var spriteData = gulp
        .src(paths_res.sprites.jpg)
        .pipe(
            cache(
                imagemin([imagemin.mozjpeg({ quality: 95, progressive: true })])
            )
        )
        .pipe(
            spritesmith({
                imgName: "sprite_jpg.jpg",
                cssName: "sprite_jpg.scss",
                imgPath: paths_assets.sprites.jpg,
                imgOpts: { quality: 100 },
                padding: 5,
                algorithm: "binary-tree"
            })
        );

    spriteData.img.pipe(gulp.dest(paths_assets.sprites.jpg)); // путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest(paths_src.sprites.jpg)); // путь, куда сохраняем стили
    done();
}

function sprite_svg_build() {
    return (
        gulp
            .src(paths_res.sprites.svg)
            // .pipe(debug())
            .pipe(
                svgSprite({
                    preview: false,
                    mode: "symbols",
                    templates: { scss: true },
                    selector: "icon_svg__%f",
                    svg: {
                        symbols: "symbol_sprite.html"
                    },
                    svgPath: "%f",
                    cssFile: "_sprite.scss",
                    common: "icon_svg"
                })
            )
            // .pipe(
            //     cheerio({
            //         run: function($) {
            //             $("path").each(function() {
            //                 if ($(this).attr('stroke') === undefined) {
            //                     $(this).attr('stroke', "none");
            //                 } else {
            //                     $(this).removeAttr('stroke');
            //                 }
            //                 if ($(this).attr('fill') != "none") {
            //                     $(this).removeAttr('fill');
            //                 }
            //             });
            //         },
            //         parserOptions: {xmlMode: true},
            //     })
            // )
            .pipe(gulp.dest(paths_assets.sprites.svg))
            .on("error", gutil.log)
    );
}

function sprite_svg_sass() {
    return (
        gulp
            .src(paths_res.sprites.svg)
            // .pipe(debug())
            .pipe(
                svgSprite({
                    selector: "icon_svg__%f",
                    common: "icon_svg",
                    svg: {
                        sprite: "svg_sprite.html"
                    },
                    cssFile: "../../../../src/sprites/svg/_svg_sprite.scss",
                    templates: {
                        css: require("fs").readFileSync(
                            paths_src.sprites.svg_template,
                            "utf-8"
                        )
                    }
                })
            )
            .pipe(gulp.dest(paths_assets.sprites.svg))
            .on("error", gutil.log)
    );
}

//===========================================
//============== Fonts ======================
//===========================================

function fonts_clean() {
    return item_clean(paths_assets.fonts);
}

function fonts_build() {
    return item_mv(paths_res.fonts, paths_assets.fonts);
}

function vendor_build() {
    return item_mv(paths_src.vendor, paths_assets.vendor);
}

//===========================================
//============== Images =====================
//===========================================

function img_clean() {
    return item_clean(paths_assets.img);
}

function img_build() {
    return gulp
        .src(paths_res.images)
        .pipe(
            cache(
                imagemin([
                    imagemin.gifsicle({ interlaced: true }),
                    imagemin.mozjpeg({ quality: 90, progressive: true }),
                    imagemin.optipng({ optimizationLevel: 7 }),
                    imagemin.svgo({
                        plugins: [
                            { removeViewBox: true },
                            { cleanupIDs: false }
                        ]
                    })
                ])
            )
        )
        .pipe(gulp.dest(paths_assets.img))
        .on("error", gutil.log);
}

function webp_build() {
    return gulp
        .src(paths_res.images)
        .pipe(cache(webp({ quality: 90 })))
        .pipe(rename({ extname: ".webp" }))
        .pipe(gulp.dest(paths_assets.img))
        .on("error", gutil.log);
}

//===========================================
//============== Favicon ====================
//===========================================

function favicon_clean() {
    return item_clean(paths_assets.favicon);
}

function favicon_build() {
    return item_mv(paths_res.favicon, paths_assets.favicon);
}

//===========================================
//=============== Task ======================
//===========================================

//............... SASS ....................
gulp.task("sass:clean", sass_clean);
gulp.task("sass:build", gulp.series(sass_clean, sass_build, sass_mv));
gulp.task("sass", gulp.series(sass_clean, sass_build));

//............. Site JS ..................
gulp.task("js:clean", js_clean);
gulp.task("js:build", gulp.parallel(js_build, js_min, js_mv, vendor_build));
gulp.task("js", gulp.series(js_clean, "js:build"));

//............... HTML ..................
gulp.task(
    "revision:all",
    gulp.series(
        revision_clear,
        js_clean,
        "sass:build",
        js_build,
        js_min,
        js_mv,
        vendor_build,
        revision_js,
        revision_css
    )
);
gulp.task("html:clean", gulp.parallel(templates_clean));
gulp.task(
    "html:build",
    gulp.series(
        "revision:all",
        gulp.parallel(chunks_build, chunks_fenom_build, templates_build)
    )
);
gulp.task("html", gulp.series("html:clean", "html:build"));

gulp.task(
    "html:watch",
    gulp.series(
        "html:build",
        gulp.series(browserSync, gulp.parallel(templates_watch))
    )
);

gulp.task("sass:watch", gulp.series(sass_build, browserSync, sass_watch));
gulp.task(
    "js:watch",
    gulp.series(js_build, js_mv, vendor_build, js_min, browserSync, js_watch)
);

//.............. Chunks ...................
gulp.task("chunks:clean", chunks_clean);
gulp.task("chunks:build", gulp.series("revision:all", chunks_build));
gulp.task(
    "chunks",
    gulp.series(chunks_clean, chunks_build, chunks_fenom_build)
);
gulp.task(
    "chunks:watch",
    gulp.series("chunks:build", browserSync, chunks_watch)
);
gulp.task("scripts", gulp.series("js"));

//............. Sprites .................
gulp.task("sprites_jpg:clean", sprite_jpg_clean);
gulp.task("sprites_png:clean", sprite_png_clean);
gulp.task("sprites_svg:clean", sprite_svg_clean);

gulp.task("sprites_jpg", sprite_jpg);
gulp.task("sprites_png", sprite_png);
gulp.task("sprites_svg:build", sprite_svg_build);
gulp.task("sprites_svg:sass", sprite_svg_sass);
gulp.task("sprites_svg", gulp.series(sprite_svg_build, sprite_svg_sass));

gulp.task(
    "sprites:build",
    gulp.parallel(sprite_jpg, sprite_png, "sprites_svg")
);
gulp.task(
    "sprites:clean",
    gulp.parallel(sprite_jpg_clean, sprite_png_clean, sprite_svg_clean)
);

//.............. Fonts ..................
gulp.task("fonts:clean", fonts_clean);
gulp.task("fonts:build", fonts_build);
gulp.task("fonts", gulp.series(fonts_clean, fonts_build));

//.............. Images ..................
gulp.task("img:clean", img_clean);
gulp.task("img:build", img_build);
gulp.task("webp:build", webp_build);
gulp.task("img", gulp.parallel(img_build, webp_build));

//............. Favicon .................
gulp.task("favicon:clean", favicon_clean);
gulp.task("favicon:build", favicon_build);
gulp.task("favicon", gulp.series(favicon_clean, favicon_build));

//............... ALL ....................
//'sprites:build'
gulp.task(
    "build",
    gulp.series(
        gulp.parallel("img", favicon_build, fonts_build, "sprites:build"),
        "html:build"
    )
);

gulp.task(
    "js2:build",
    gulp.series(
        // gulp.parallel(vendor_build),
        gulp.parallel(js_build, js_min, js_mv, vendor_build)
    )
);

gulp.task(
    "pre-build",
    gulp.parallel("img", favicon_build, fonts_build, "sprites:build")
);

gulp.task(
    "clean",
    gulp.parallel(
        templates_clean,
        chunks_clean,
        sass_clean,
        js_clean,
        "sprites:clean",
        fonts_clean,
        img_clean,
        favicon_clean
    )
);

gulp.task("cache:clear", cache.clearAll);

gulp.task(
    "watch",
    gulp.series(
        "build",
        browserSync,
        gulp.parallel(
            data_watch,
            sass_mv,
            templates_watch,
            chunks_watch,
            sass_watch,
            js_watch
        )
    )
);

gulp.task("default", gulp.series("watch"));

function revision_clear() {
    return item_clean(path_project + manifest_filename);
}

function revision_css() {
    return gulp
        .src([`${paths_assets.css}*.css`, `${paths_assets.css}/**/*.css`])
        .pipe(rev())
        .pipe(gulp.dest(paths_assets.css))
        .pipe(
            rev.manifest({
                base: paths_localhost.assets,
                path: manifest_filename,
                merge: true
            })
        )
        .pipe(gulp.dest(paths_localhost.assets));
}

function revision_js() {
    return gulp
        .src([`${paths_assets.js}/*js`])
        .pipe(rev())
        .pipe(gulp.dest(paths_assets.js))
        .pipe(
            rev.manifest({
                base: paths_localhost.assets,
                path: manifest_filename,
                merge: true
            })
        )
        .pipe(gulp.dest(paths_localhost.assets));
}
