# glokurs-landing

<!-- ### Проблемы / не сделано: -->

### Шаблонизатор:

[Nunjucks](https://mozilla.github.io/nunjucks/api.html) - в данном случае сбирается gulp-ом


### Редактирование:

Если проект собирать gulp'ом, то Все данные можно править в файле: `/src/data/data.html`
И исходники в директории `/src`

Иначе, можно править уже собранный проект в `/app/index.html`

**Не забудьте про мета-теги в `<head>`**

Для сборки проекта можно запустить задачу `gulp build` или просто `gulp` (`gulp
watch`);  

`gulp watch` - соберет проект и запустит сервер для разработки
[Browsersync](https://browsersync.io/), 
**Browsersync** настроен на открытие сайта в браузере **Chromium**  - это можно
изменить в `gulpfile.js` на строке `136`

Версии:  

* node: v12.16.  
* npm: 6.14.2


### Публикация: 

Все файлы для публикации сайта находятся в директории: `/app`

Соответственно, `index.html` - собранная, главная страница
`index.min.html` - ее минифицировання версия


### Контакты: 

* E-mail:
  [kvanton.web@yandex.ru](mailto:kvanton.web@yandex.ru?subject=GloKurs-landing)  
* Telegram: [@KVAnton](https://t.me/KVAnton)


### Результаты оптимизации [PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights/?hl=RU)

![Mobile](https://i.imgur.com/Upjy3wG.png "Mobile 100%")

![Desktop](https://i.imgur.com/9zQEuKc.png "Desktop 100%")



