$(document).ready(function(){
    var index = 0;

    $('.s9_slider_links').on('setPosition', function(event, slick) {
        $('.s9_slider_links .slick-list')[0].style.height = 'auto';
        $('.s9_slider_links .slick-track')[0].style.height = 'auto';
        $('.s9_slider_links .slick-track')[0].style.transform = 'none';
    });


    $('.s9_slider_links').on('init', function(event, slick) {
        try {
            index = $('.s9_slider .slider-active').closest('.slick-slide').data('slick-index');
            if (index) {
                setTimeout(function() {
                    slick.slickGoTo(index);
                }, 0);
            }
        } catch (err) {
            console.log(err)
        }
    });

    $('.s9_slider').slick({
        asNavFor: '.s9_slider_links',
        lazyLoad: 'progressive',
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 1,
        arrows: false,
        vertical: true,
        verticalSwiping: true
    });

    $('.s9_slider_links').slick({
        asNavFor: '.s9_slider',
        centerMode: false,
        slidesToShow: 10,
        slidesToScroll: 1,
        arrows: false,
        vertical: true,
        verticalSwiping: true,
        infinite: false,
        focusOnSelect: true
    });
});
