(function() {
    var $root = $("html, body");

    $('a[href^="#"]').click(function() {
        $root.animate(
            {
                scrollTop: $($.attr(this, "href")).offset().top
            },
            500
        );

        return false;
    });
})();

$(function () {
    $(document).scroll(function () {
        var $nav = $(".kw-navbar");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
    });
});
