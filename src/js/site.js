;
var webp = false;
Modernizr.on('webp', function(result) {
    $('[data-bg_webp]').each(function(index) {
        let img_name_jpg = $(this).data('bg_jpg');
        let img_name_webp = $(this).data('bg_webp');
        if (result == true && img_name_webp) {
            $(this).css('background-image', `url(${img_name_webp})`);
        } else {
            $(this).css('background-image', `url(${img_name_jpg})`);
        }
    });
});

(() => {
//     document.querySelectorAll();

var lazyLoadInstance = new LazyLoad({
    elements_selector: ".kw-lazy"
});

})()

$(function () {
    $(document).scroll(function () {
        var iframes = $(".kw-iframe-lazy");
        iframes.each((index, el) => {
            var src = $( el ).data('src');
            $( el ).attr("src", src);
            $( el ).removeClass("kw-iframe-lazy");
        });
    });
});
