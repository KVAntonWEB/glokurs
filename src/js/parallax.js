// var currentX = "";
// var currentY = "";
// var movementConstant = 0.015;
var prevDate = 0; // keep this as a global variable
var kw_disable = false;

// put the following inside the mousemove function
(function() {
    var scrollDebonce = 0;
    var dh = document.documentElement.clientHeight;
    // Add event listener
    const par_small = document.querySelector("#kw-parallax .s1__rhomb_small");
    const par_big = document.querySelector("#kw-parallax .s1__rhomb_big");
    document.addEventListener("scroll", () => {
        if (scrollDebonce == 0) {
            scrollDebonce = 1;
            if (false == kw_disable && window.scrollY > dh) {
                kw_disable = true;
                document.removeEventListener("mousemove", kwParallax);
            } else if (true == kw_disable && window.scrollY < dh) {
                document.addEventListener("mousemove", kwParallax);
                kw_disable = false;
            }
            setTimeout(() => {
                scrollDebonce = 0;
            }, 60);
        }
    });

    function kwParallax(e){
        if (prevDate == 0) {
            prevDate = 1;
            parallax(e, par_small, 0);
            parallax(e, par_big, 1);
            setTimeout(() => {
                prevDate = 0;
            }, 15);
        }
    }
    document.addEventListener("mousemove", kwParallax);
    // Magic happens here
    function parallax(e, elem, side) {
        let _w = window.innerWidth / 2;
        let _h = window.innerHeight / 2;
        let _mouseX = e.clientX;
        let _mouseY = e.clientY;
        let _depth1 = `${50 - (_mouseX - _w) * 0.01}% ${50 -
            (_mouseY - _h) * 0.01}%`;
        let _depth2 = `${50 - (_mouseX - _w) * 0.02}% ${50 -
            (_mouseY - _h) * 0.02}%`;
        let _depth3 = `${50 - (_mouseX - _w) * 0.06}% ${50 -
            (_mouseY - _h) * 0.06}%`;
        let x = `${_depth3}, ${_depth2}, ${_depth1}`;
        // console.log(x);
        if (side == 0) {
            elem.style.left = `${-5 - (_mouseX - _w) * 0.01}%`;
            elem.style.top = `${20 - (_mouseY - _h) * 0.01}%`;
        } else if (side == 1) {
            elem.style.right = `${-23 - (_mouseX - _w) * 0.01}%`;
            elem.style.bottom = `${5 - (_mouseY - _h) * 0.02}%`;
        }
    }
})();

// $(document).mousemove(function(e) {
//     var date = new Date().getTime();
//     if (date - prevDate > 15) {
//         prevDate = date;
//         if (currentX == "") currentX = e.pageX;
//         var xdiff = e.pageX - currentX;
//         currentX = e.pageX;
//         if (currentY == "") currentY = e.pageY;
//         var ydiff = e.pageY - currentY;
//         currentY = e.pageY;

//         $(".parallax div").each(function(i, el) {
//             var movement = (i + 1) * (xdiff * movementConstant);
//             var movementy = (i + 1) * (ydiff * movementConstant);
//             var newX = $(el).position().left + movement;
//             var newY = $(el).position().top + movementy;
//             $(el).css("left", newX + "px");
//             $(el).css("top", newY + "px");
//         });
//     }
// });

(() => {
    $(".par_glare").tilt({
        glare: true,
        maxGlare: 0.5
    });

    $(".par_x").tilt({
        axis: "x"
    });

    $(".par_y").tilt({
        axis: "y"
    });

    $(".par_scale").tilt({
        scale: 1.1
    });

    $(".par_btn").tilt({
        scale: 1.1,
        glare: true,
        maxGlare: 0.5
    });

    $(".par_card").tilt({
        axis: "x",
        glare: true,
        maxGlare: 0.5,
        scale: 1.05
    });
})();
