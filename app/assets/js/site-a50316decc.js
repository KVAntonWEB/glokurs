"use strict";

function _typeof(obj) {
  "@babel/helpers - typeof";
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj &&
        typeof Symbol === "function" &&
        obj.constructor === Symbol &&
        obj !== Symbol.prototype
        ? "symbol"
        : typeof obj;
    };
  }
  return _typeof(obj);
}

!(function(t, e) {
  "object" ==
    (typeof exports === "undefined" ? "undefined" : _typeof(exports)) &&
  "undefined" != typeof module
    ? (module.exports = e())
    : "function" == typeof define && define.amd
    ? define(e)
    : ((t = t || self).LazyLoad = e());
})(void 0, function() {
  "use strict";

  function t() {
    return (t =
      Object.assign ||
      function(t) {
        for (var e = 1; e < arguments.length; e++) {
          var n = arguments[e];

          for (var r in n) {
            Object.prototype.hasOwnProperty.call(n, r) && (t[r] = n[r]);
          }
        }

        return t;
      }).apply(this, arguments);
  }

  var e = "undefined" != typeof window,
    n =
      (e && !("onscroll" in window)) ||
      ("undefined" != typeof navigator &&
        /(gle|ing|ro)bot|crawl|spider/i.test(navigator.userAgent)),
    r = e && "IntersectionObserver" in window,
    a = e && "classList" in document.createElement("p"),
    o = {
      elements_selector: "img",
      container: n || e ? document : null,
      threshold: 300,
      thresholds: null,
      data_src: "src",
      data_srcset: "srcset",
      data_sizes: "sizes",
      data_bg: "bg",
      data_poster: "poster",
      class_loading: "loading",
      class_loaded: "loaded",
      class_error: "error",
      load_delay: 0,
      auto_unobserve: !0,
      callback_enter: null,
      callback_exit: null,
      callback_reveal: null,
      callback_loaded: null,
      callback_error: null,
      callback_finish: null,
      use_native: !1
    },
    i = function i(t, e) {
      var n,
        r = new t(e);

      try {
        n = new CustomEvent("LazyLoad::Initialized", {
          detail: {
            instance: r
          }
        });
      } catch (t) {
        (n = document.createEvent("CustomEvent")).initCustomEvent(
          "LazyLoad::Initialized",
          !1,
          !1,
          {
            instance: r
          }
        );
      }

      window.dispatchEvent(n);
    },
    s = function s(t, e) {
      return t.getAttribute("data-" + e);
    },
    c = function c(t, e, n) {
      var r = "data-" + e;
      null !== n ? t.setAttribute(r, n) : t.removeAttribute(r);
    },
    l = function l(t) {
      return "true" === s(t, "was-processed");
    },
    u = function u(t, e) {
      return c(t, "ll-timeout", e);
    },
    d = function d(t) {
      return s(t, "ll-timeout");
    },
    f = function f(t) {
      for (var e, n = [], r = 0; (e = t.children[r]); r += 1) {
        "SOURCE" === e.tagName && n.push(e);
      }

      return n;
    },
    _ = function _(t, e, n) {
      n && t.setAttribute(e, n);
    },
    v = function v(t, e) {
      _(t, "sizes", s(t, e.data_sizes)),
        _(t, "srcset", s(t, e.data_srcset)),
        _(t, "src", s(t, e.data_src));
    },
    g = {
      IMG: function IMG(t, e) {
        var n = t.parentNode;
        n &&
          "PICTURE" === n.tagName &&
          f(n).forEach(function(t) {
            v(t, e);
          });
        v(t, e);
      },
      IFRAME: function IFRAME(t, e) {
        _(t, "src", s(t, e.data_src));
      },
      VIDEO: function VIDEO(t, e) {
        f(t).forEach(function(t) {
          _(t, "src", s(t, e.data_src));
        }),
          _(t, "poster", s(t, e.data_poster)),
          _(t, "src", s(t, e.data_src)),
          t.load();
      }
    },
    h = function h(t, e) {
      var n,
        r,
        a = e._settings,
        o = t.tagName,
        i = g[o];
      if (i)
        return (
          i(t, a),
          (e.loadingCount += 1),
          void (e._elements =
            ((n = e._elements),
            (r = t),
            n.filter(function(t) {
              return t !== r;
            })))
        );
      !(function(t, e) {
        var n = s(t, e.data_src),
          r = s(t, e.data_bg);
        n && (t.style.backgroundImage = 'url("'.concat(n, '")')),
          r && (t.style.backgroundImage = r);
      })(t, a);
    },
    m = function m(t, e) {
      a ? t.classList.add(e) : (t.className += (t.className ? " " : "") + e);
    },
    b = function b(t, e) {
      a
        ? t.classList.remove(e)
        : (t.className = t.className
            .replace(new RegExp("(^|\\s+)" + e + "(\\s+|$)"), " ")
            .replace(/^\s+/, "")
            .replace(/\s+$/, ""));
    },
    p = function p(t, e, n, r) {
      t && (void 0 === r ? (void 0 === n ? t(e) : t(e, n)) : t(e, n, r));
    },
    y = function y(t, e, n) {
      t.addEventListener(e, n);
    },
    E = function E(t, e, n) {
      t.removeEventListener(e, n);
    },
    w = function w(t, e, n) {
      E(t, "load", e), E(t, "loadeddata", e), E(t, "error", n);
    },
    I = function I(t, e, n) {
      var r = n._settings,
        a = e ? r.class_loaded : r.class_error,
        o = e ? r.callback_loaded : r.callback_error,
        i = t.target;
      b(i, r.class_loading),
        m(i, a),
        p(o, i, n),
        (n.loadingCount -= 1),
        0 === n._elements.length &&
          0 === n.loadingCount &&
          p(r.callback_finish, n);
    },
    k = function k(t, e) {
      var n = function n(a) {
          I(a, !0, e), w(t, n, r);
        },
        r = function r(a) {
          I(a, !1, e), w(t, n, r);
        };

      !(function(t, e, n) {
        y(t, "load", e), y(t, "loadeddata", e), y(t, "error", n);
      })(t, n, r);
    },
    A = ["IMG", "IFRAME", "VIDEO"],
    L = function L(t, e) {
      var n = e._observer;
      z(t, e), n && e._settings.auto_unobserve && n.unobserve(t);
    },
    z = function z(t, e, n) {
      var r = e._settings;
      (!n && l(t)) ||
        (A.indexOf(t.tagName) > -1 && (k(t, e), m(t, r.class_loading)),
        h(t, e),
        (function(t) {
          c(t, "was-processed", "true");
        })(t),
        p(r.callback_reveal, t, e));
    },
    O = function O(t) {
      var e = d(t);
      e && (clearTimeout(e), u(t, null));
    },
    N = function N(t, e, n) {
      var r = n._settings;
      p(r.callback_enter, t, e, n),
        r.load_delay
          ? (function(t, e) {
              var n = e._settings.load_delay,
                r = d(t);
              r ||
                ((r = setTimeout(function() {
                  L(t, e), O(t);
                }, n)),
                u(t, r));
            })(t, n)
          : L(t, n);
    },
    C = function C(t) {
      return (
        !!r &&
        ((t._observer = new IntersectionObserver(
          function(e) {
            e.forEach(function(e) {
              return (function(t) {
                return t.isIntersecting || t.intersectionRatio > 0;
              })(e)
                ? N(e.target, e, t)
                : (function(t, e, n) {
                    var r = n._settings;
                    p(r.callback_exit, t, e, n), r.load_delay && O(t);
                  })(e.target, e, t);
            });
          },
          {
            root: (e = t._settings).container === document ? null : e.container,
            rootMargin: e.thresholds || e.threshold + "px"
          }
        )),
        !0)
      );
      var e;
    },
    x = ["IMG", "IFRAME"],
    M = function M(t) {
      return Array.prototype.slice.call(t);
    },
    R = function R(t, e) {
      return (function(t) {
        return t.filter(function(t) {
          return !l(t);
        });
      })(
        M(
          t ||
            (function(t) {
              return t.container.querySelectorAll(t.elements_selector);
            })(e)
        )
      );
    },
    T = function T(t) {
      var e = t._settings,
        n = e.container.querySelectorAll("." + e.class_error);
      M(n).forEach(function(t) {
        b(t, e.class_error),
          (function(t) {
            c(t, "was-processed", null);
          })(t);
      }),
        t.update();
    },
    j = function j(n, r) {
      var a;
      (this._settings = (function(e) {
        return t({}, o, e);
      })(n)),
        (this.loadingCount = 0),
        C(this),
        this.update(r),
        (a = this),
        e &&
          window.addEventListener("online", function(t) {
            T(a);
          });
    };

  return (
    (j.prototype = {
      update: function update(t) {
        var e,
          r = this,
          a = this._settings;
        ((this._elements = R(t, a)), !n && this._observer)
          ? ((function(t) {
              return t.use_native && "loading" in HTMLImageElement.prototype;
            })(a) &&
              ((e = this)._elements.forEach(function(t) {
                -1 !== x.indexOf(t.tagName) &&
                  (t.setAttribute("loading", "lazy"), z(t, e));
              }),
              (this._elements = R(t, a))),
            this._elements.forEach(function(t) {
              r._observer.observe(t);
            }))
          : this.loadAll();
      },
      destroy: function destroy() {
        var t = this;
        this._observer &&
          (this._elements.forEach(function(e) {
            t._observer.unobserve(e);
          }),
          (this._observer = null)),
          (this._elements = null),
          (this._settings = null);
      },
      load: function load(t, e) {
        z(t, this, e);
      },
      loadAll: function loadAll() {
        var t = this;

        this._elements.forEach(function(e) {
          L(e, t);
        });
      }
    }),
    e &&
      (function(t, e) {
        if (e)
          if (e.length)
            for (var n, r = 0; (n = e[r]); r += 1) {
              i(t, n);
            }
          else i(t, e);
      })(j, window.lazyLoadOptions),
    j
  );
});
("use strict");

function _typeof2(obj) {
  "@babel/helpers - typeof";
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof2 = function _typeof2(obj) {
      return typeof obj;
    };
  } else {
    _typeof2 = function _typeof2(obj) {
      return obj &&
        typeof Symbol === "function" &&
        obj.constructor === Symbol &&
        obj !== Symbol.prototype
        ? "symbol"
        : typeof obj;
    };
  }
  return _typeof2(obj);
}

var _typeof =
  "function" == typeof Symbol && "symbol" == _typeof2(Symbol.iterator)
    ? function(t) {
        return _typeof2(t);
      }
    : function(t) {
        return t &&
          "function" == typeof Symbol &&
          t.constructor === Symbol &&
          t !== Symbol.prototype
          ? "symbol"
          : _typeof2(t);
      };

!(function(t) {
  "function" == typeof define && define.amd
    ? define(["jquery"], t)
    : "object" ===
        ("undefined" == typeof module ? "undefined" : _typeof(module)) &&
      module.exports
    ? (module.exports = function(i, s) {
        return (
          void 0 === s &&
            (s =
              "undefined" != typeof window
                ? require("jquery")
                : require("jquery")(i)),
          t(s),
          s
        );
      })
    : t(jQuery);
})(function(t) {
  return (
    (t.fn.tilt = function(i) {
      var s = function s() {
          this.ticking ||
            (requestAnimationFrame(g.bind(this)), (this.ticking = !0));
        },
        e = function e() {
          var i = this;
          t(this).on("mousemove", o),
            t(this).on("mouseenter", a),
            this.settings.reset && t(this).on("mouseleave", l),
            this.settings.glare && t(window).on("resize", d.bind(i));
        },
        n = function n() {
          var i = this;
          void 0 !== this.timeout && clearTimeout(this.timeout),
            t(this).css({
              transition: this.settings.speed + "ms " + this.settings.easing
            }),
            this.settings.glare &&
              this.glareElement.css({
                transition:
                  "opacity " +
                  this.settings.speed +
                  "ms " +
                  this.settings.easing
              }),
            (this.timeout = setTimeout(function() {
              t(i).css({
                transition: ""
              }),
                i.settings.glare &&
                  i.glareElement.css({
                    transition: ""
                  });
            }, this.settings.speed));
        },
        a = function a(i) {
          (this.ticking = !1),
            t(this).css({
              "will-change": "transform"
            }),
            n.call(this),
            t(this).trigger("tilt.mouseEnter");
        },
        r = function r(i) {
          return (
            "undefined" == typeof i &&
              (i = {
                pageX: t(this).offset().left + t(this).outerWidth() / 2,
                pageY: t(this).offset().top + t(this).outerHeight() / 2
              }),
            {
              x: i.pageX,
              y: i.pageY
            }
          );
        },
        o = function o(t) {
          (this.mousePositions = r(t)), s.call(this);
        },
        l = function l() {
          n.call(this),
            (this.reset = !0),
            s.call(this),
            t(this).trigger("tilt.mouseLeave");
        },
        h = function h() {
          var i = t(this).outerWidth(),
            s = t(this).outerHeight(),
            e = t(this).offset().left,
            n = t(this).offset().top,
            a = (this.mousePositions.x - e) / i,
            r = (this.mousePositions.y - n) / s,
            o = (this.settings.maxTilt / 2 - a * this.settings.maxTilt).toFixed(
              2
            ),
            l = (r * this.settings.maxTilt - this.settings.maxTilt / 2).toFixed(
              2
            ),
            h =
              Math.atan2(
                this.mousePositions.x - (e + i / 2),
                -(this.mousePositions.y - (n + s / 2))
              ) *
              (180 / Math.PI);
          return {
            tiltX: o,
            tiltY: l,
            percentageX: 100 * a,
            percentageY: 100 * r,
            angle: h
          };
        },
        g = function g() {
          return (
            (this.transforms = h.call(this)),
            this.reset
              ? ((this.reset = !1),
                t(this).css(
                  "transform",
                  "perspective(" +
                    this.settings.perspective +
                    "px) rotateX(0deg) rotateY(0deg)"
                ),
                void (
                  this.settings.glare &&
                  (this.glareElement.css(
                    "transform",
                    "rotate(180deg) translate(-50%, -50%)"
                  ),
                  this.glareElement.css("opacity", "0"))
                ))
              : (t(this).css(
                  "transform",
                  "perspective(" +
                    this.settings.perspective +
                    "px) rotateX(" +
                    ("x" === this.settings.disableAxis
                      ? 0
                      : this.transforms.tiltY) +
                    "deg) rotateY(" +
                    ("y" === this.settings.disableAxis
                      ? 0
                      : this.transforms.tiltX) +
                    "deg) scale3d(" +
                    this.settings.scale +
                    "," +
                    this.settings.scale +
                    "," +
                    this.settings.scale +
                    ")"
                ),
                this.settings.glare &&
                  (this.glareElement.css(
                    "transform",
                    "rotate(" +
                      this.transforms.angle +
                      "deg) translate(-50%, -50%)"
                  ),
                  this.glareElement.css(
                    "opacity",
                    "" +
                      (this.transforms.percentageY * this.settings.maxGlare) /
                        100
                  )),
                t(this).trigger("change", [this.transforms]),
                void (this.ticking = !1))
          );
        },
        c = function c() {
          var i = this.settings.glarePrerender;

          if (
            (i ||
              t(this).append(
                '<div class="js-tilt-glare"><div class="js-tilt-glare-inner"></div></div>'
              ),
            (this.glareElementWrapper = t(this).find(".js-tilt-glare")),
            (this.glareElement = t(this).find(".js-tilt-glare-inner")),
            !i)
          ) {
            var s = {
              position: "absolute",
              top: "0",
              left: "0",
              width: "100%",
              height: "100%"
            };
            this.glareElementWrapper.css(s).css({
              overflow: "hidden",
              "pointer-events": "none"
            }),
              this.glareElement.css({
                position: "absolute",
                top: "50%",
                left: "50%",
                "background-image":
                  "linear-gradient(0deg, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%)",
                width: "" + 2 * t(this).outerWidth(),
                height: "" + 2 * t(this).outerWidth(),
                transform: "rotate(180deg) translate(-50%, -50%)",
                "transform-origin": "0% 0%",
                opacity: "0"
              });
          }
        },
        d = function d() {
          this.glareElement.css({
            width: "" + 2 * t(this).outerWidth(),
            height: "" + 2 * t(this).outerWidth()
          });
        };

      return (
        (t.fn.tilt.destroy = function() {
          t(this).each(function() {
            t(this)
              .find(".js-tilt-glare")
              .remove(),
              t(this).css({
                "will-change": "",
                transform: ""
              }),
              t(this).off("mousemove mouseenter mouseleave");
          });
        }),
        (t.fn.tilt.getValues = function() {
          var i = [];
          return (
            t(this).each(function() {
              (this.mousePositions = r.call(this)), i.push(h.call(this));
            }),
            i
          );
        }),
        (t.fn.tilt.reset = function() {
          t(this).each(function() {
            var i = this;
            (this.mousePositions = r.call(this)),
              (this.settings = t(this).data("settings")),
              l.call(this),
              setTimeout(function() {
                i.reset = !1;
              }, this.settings.transition);
          });
        }),
        this.each(function() {
          var s = this;
          (this.settings = t.extend(
            {
              maxTilt: t(this).is("[data-tilt-max]")
                ? t(this).data("tilt-max")
                : 20,
              perspective: t(this).is("[data-tilt-perspective]")
                ? t(this).data("tilt-perspective")
                : 300,
              easing: t(this).is("[data-tilt-easing]")
                ? t(this).data("tilt-easing")
                : "cubic-bezier(.03,.98,.52,.99)",
              scale: t(this).is("[data-tilt-scale]")
                ? t(this).data("tilt-scale")
                : "1",
              speed: t(this).is("[data-tilt-speed]")
                ? t(this).data("tilt-speed")
                : "400",
              transition:
                !t(this).is("[data-tilt-transition]") ||
                t(this).data("tilt-transition"),
              disableAxis: t(this).is("[data-tilt-disable-axis]")
                ? t(this).data("tilt-disable-axis")
                : null,
              axis: t(this).is("[data-tilt-axis]")
                ? t(this).data("tilt-axis")
                : null,
              reset:
                !t(this).is("[data-tilt-reset]") || t(this).data("tilt-reset"),
              glare:
                !!t(this).is("[data-tilt-glare]") && t(this).data("tilt-glare"),
              maxGlare: t(this).is("[data-tilt-maxglare]")
                ? t(this).data("tilt-maxglare")
                : 1
            },
            i
          )),
            null !== this.settings.axis &&
              (console.warn(
                "Tilt.js: the axis setting has been renamed to disableAxis. See https://github.com/gijsroge/tilt.js/pull/26 for more information"
              ),
              (this.settings.disableAxis = this.settings.axis)),
            (this.init = function() {
              t(s).data("settings", s.settings),
                s.settings.glare && c.call(s),
                e.call(s);
            }),
            this.init();
        })
      );
    }),
    t("[data-tilt]").tilt(),
    !0
  );
});
("use strict");

function _typeof(obj) {
  "@babel/helpers - typeof";
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj &&
        typeof Symbol === "function" &&
        obj.constructor === Symbol &&
        obj !== Symbol.prototype
        ? "symbol"
        : typeof obj;
    };
  }
  return _typeof(obj);
}

/*! modernizr 3.6.0 (Custom Build) | MIT *
 * https://modernizr.com/download/?-webp-setclasses !*/
!(function(e, n, A) {
  function o(e, n) {
    return _typeof(e) === n;
  }

  function t() {
    var e, n, A, t, a, i, l;

    for (var f in r) {
      if (r.hasOwnProperty(f)) {
        if (
          ((e = []),
          (n = r[f]),
          n.name &&
            (e.push(n.name.toLowerCase()),
            n.options && n.options.aliases && n.options.aliases.length))
        )
          for (A = 0; A < n.options.aliases.length; A++) {
            e.push(n.options.aliases[A].toLowerCase());
          }

        for (
          t = o(n.fn, "function") ? n.fn() : n.fn, a = 0;
          a < e.length;
          a++
        ) {
          (i = e[a]),
            (l = i.split(".")),
            1 === l.length
              ? (Modernizr[l[0]] = t)
              : (!Modernizr[l[0]] ||
                  Modernizr[l[0]] instanceof Boolean ||
                  (Modernizr[l[0]] = new Boolean(Modernizr[l[0]])),
                (Modernizr[l[0]][l[1]] = t)),
            s.push((t ? "" : "no-") + l.join("-"));
        }
      }
    }
  }

  function a(e) {
    var n = u.className,
      A = Modernizr._config.classPrefix || "";

    if ((c && (n = n.baseVal), Modernizr._config.enableJSClass)) {
      var o = new RegExp("(^|\\s)" + A + "no-js(\\s|$)");
      n = n.replace(o, "$1" + A + "js$2");
    }

    Modernizr._config.enableClasses &&
      ((n += " " + A + e.join(" " + A)),
      c ? (u.className.baseVal = n) : (u.className = n));
  }

  function i(e, n) {
    if ("object" == _typeof(e))
      for (var A in e) {
        f(e, A) && i(A, e[A]);
      }
    else {
      e = e.toLowerCase();
      var o = e.split("."),
        t = Modernizr[o[0]];
      if ((2 == o.length && (t = t[o[1]]), "undefined" != typeof t))
        return Modernizr;
      (n = "function" == typeof n ? n() : n),
        1 == o.length
          ? (Modernizr[o[0]] = n)
          : (!Modernizr[o[0]] ||
              Modernizr[o[0]] instanceof Boolean ||
              (Modernizr[o[0]] = new Boolean(Modernizr[o[0]])),
            (Modernizr[o[0]][o[1]] = n)),
        a([(n && 0 != n ? "" : "no-") + o.join("-")]),
        Modernizr._trigger(e, n);
    }
    return Modernizr;
  }

  var s = [],
    r = [],
    l = {
      _version: "3.6.0",
      _config: {
        classPrefix: "",
        enableClasses: !0,
        enableJSClass: !0,
        usePrefixes: !0
      },
      _q: [],
      on: function on(e, n) {
        var A = this;
        setTimeout(function() {
          n(A[e]);
        }, 0);
      },
      addTest: function addTest(e, n, A) {
        r.push({
          name: e,
          fn: n,
          options: A
        });
      },
      addAsyncTest: function addAsyncTest(e) {
        r.push({
          name: null,
          fn: e
        });
      }
    },
    Modernizr = function Modernizr() {};

  (Modernizr.prototype = l), (Modernizr = new Modernizr());
  var f,
    u = n.documentElement,
    c = "svg" === u.nodeName.toLowerCase();
  !(function() {
    var e = {}.hasOwnProperty;
    f =
      o(e, "undefined") || o(e.call, "undefined")
        ? function(e, n) {
            return n in e && o(e.constructor.prototype[n], "undefined");
          }
        : function(n, A) {
            return e.call(n, A);
          };
  })(),
    (l._l = {}),
    (l.on = function(e, n) {
      this._l[e] || (this._l[e] = []),
        this._l[e].push(n),
        Modernizr.hasOwnProperty(e) &&
          setTimeout(function() {
            Modernizr._trigger(e, Modernizr[e]);
          }, 0);
    }),
    (l._trigger = function(e, n) {
      if (this._l[e]) {
        var A = this._l[e];
        setTimeout(function() {
          var e, o;

          for (e = 0; e < A.length; e++) {
            (o = A[e])(n);
          }
        }, 0),
          delete this._l[e];
      }
    }),
    Modernizr._q.push(function() {
      l.addTest = i;
    }),
    Modernizr.addAsyncTest(function() {
      function e(e, n, A) {
        function o(n) {
          var o = n && "load" === n.type ? 1 == t.width : !1,
            a = "webp" === e;
          i(e, a && o ? new Boolean(o) : o), A && A(n);
        }

        var t = new Image();
        (t.onerror = o), (t.onload = o), (t.src = n);
      }

      var n = [
          {
            uri:
              "data:image/webp;base64,UklGRiQAAABXRUJQVlA4IBgAAAAwAQCdASoBAAEAAwA0JaQAA3AA/vuUAAA=",
            name: "webp"
          },
          {
            uri:
              "data:image/webp;base64,UklGRkoAAABXRUJQVlA4WAoAAAAQAAAAAAAAAAAAQUxQSAwAAAABBxAR/Q9ERP8DAABWUDggGAAAADABAJ0BKgEAAQADADQlpAADcAD++/1QAA==",
            name: "webp.alpha"
          },
          {
            uri:
              "data:image/webp;base64,UklGRlIAAABXRUJQVlA4WAoAAAASAAAAAAAAAAAAQU5JTQYAAAD/////AABBTk1GJgAAAAAAAAAAAAAAAAAAAGQAAABWUDhMDQAAAC8AAAAQBxAREYiI/gcA",
            name: "webp.animation"
          },
          {
            uri:
              "data:image/webp;base64,UklGRh4AAABXRUJQVlA4TBEAAAAvAAAAAAfQ//73v/+BiOh/AAA=",
            name: "webp.lossless"
          }
        ],
        A = n.shift();
      e(A.name, A.uri, function(A) {
        if (A && "load" === A.type)
          for (var o = 0; o < n.length; o++) {
            e(n[o].name, n[o].uri);
          }
      });
    }),
    t(),
    a(s),
    delete l.addTest,
    delete l.addAsyncTest;

  for (var p = 0; p < Modernizr._q.length; p++) {
    Modernizr._q[p]();
  }

  e.Modernizr = Modernizr;
})(window, document);
("use strict");

// var currentX = "";
// var currentY = "";
// var movementConstant = 0.015;
var prevDate = 0; // keep this as a global variable

var kw_disable = false; // put the following inside the mousemove function

(function() {
  var scrollDebonce = 0;
  var dh = document.documentElement.clientHeight; // Add event listener

  var par_small = document.querySelector("#kw-parallax .s1__rhomb_small");
  var par_big = document.querySelector("#kw-parallax .s1__rhomb_big");
  document.addEventListener("scroll", function() {
    if (scrollDebonce == 0) {
      scrollDebonce = 1;

      if (false == kw_disable && window.scrollY > dh) {
        kw_disable = true;
        document.removeEventListener("mousemove", kwParallax);
      } else if (true == kw_disable && window.scrollY < dh) {
        document.addEventListener("mousemove", kwParallax);
        kw_disable = false;
      }

      setTimeout(function() {
        scrollDebonce = 0;
      }, 60);
    }
  });

  function kwParallax(e) {
    if (prevDate == 0) {
      prevDate = 1;
      parallax(e, par_small, 0);
      parallax(e, par_big, 1);
      setTimeout(function() {
        prevDate = 0;
      }, 15);
    }
  }

  document.addEventListener("mousemove", kwParallax); // Magic happens here

  function parallax(e, elem, side) {
    var _w = window.innerWidth / 2;

    var _h = window.innerHeight / 2;

    var _mouseX = e.clientX;
    var _mouseY = e.clientY;

    var _depth1 = ""
      .concat(50 - (_mouseX - _w) * 0.01, "% ")
      .concat(50 - (_mouseY - _h) * 0.01, "%");

    var _depth2 = ""
      .concat(50 - (_mouseX - _w) * 0.02, "% ")
      .concat(50 - (_mouseY - _h) * 0.02, "%");

    var _depth3 = ""
      .concat(50 - (_mouseX - _w) * 0.06, "% ")
      .concat(50 - (_mouseY - _h) * 0.06, "%");

    var x = ""
      .concat(_depth3, ", ")
      .concat(_depth2, ", ")
      .concat(_depth1); // console.log(x);

    if (side == 0) {
      elem.style.left = "".concat(-5 - (_mouseX - _w) * 0.01, "%");
      elem.style.top = "".concat(20 - (_mouseY - _h) * 0.01, "%");
    } else if (side == 1) {
      elem.style.right = "".concat(-23 - (_mouseX - _w) * 0.01, "%");
      elem.style.bottom = "".concat(5 - (_mouseY - _h) * 0.02, "%");
    }
  }
})(); // $(document).mousemove(function(e) {
//     var date = new Date().getTime();
//     if (date - prevDate > 15) {
//         prevDate = date;
//         if (currentX == "") currentX = e.pageX;
//         var xdiff = e.pageX - currentX;
//         currentX = e.pageX;
//         if (currentY == "") currentY = e.pageY;
//         var ydiff = e.pageY - currentY;
//         currentY = e.pageY;
//         $(".parallax div").each(function(i, el) {
//             var movement = (i + 1) * (xdiff * movementConstant);
//             var movementy = (i + 1) * (ydiff * movementConstant);
//             var newX = $(el).position().left + movement;
//             var newY = $(el).position().top + movementy;
//             $(el).css("left", newX + "px");
//             $(el).css("top", newY + "px");
//         });
//     }
// });

(function() {
  $(".par_glare").tilt({
    glare: true,
    maxGlare: 0.5
  });
  $(".par_x").tilt({
    axis: "x"
  });
  $(".par_y").tilt({
    axis: "y"
  });
  $(".par_scale").tilt({
    scale: 1.1
  });
  $(".par_btn").tilt({
    scale: 1.1,
    glare: true,
    maxGlare: 0.5
  });
  $(".par_card").tilt({
    axis: "x",
    glare: true,
    maxGlare: 0.5,
    scale: 1.05
  });
})();
("use strict");

var webp = false;
Modernizr.on("webp", function(result) {
  $("[data-bg_webp]").each(function(index) {
    var img_name_jpg = $(this).data("bg_jpg");
    var img_name_webp = $(this).data("bg_webp");

    if (result == true && img_name_webp) {
      $(this).css("background-image", "url(".concat(img_name_webp, ")"));
    } else {
      $(this).css("background-image", "url(".concat(img_name_jpg, ")"));
    }
  });
});

(function() {
  //     document.querySelectorAll();
  var lazyLoadInstance = new LazyLoad({
    elements_selector: ".kw-lazy"
  });
})();

$(function() {
  $(document).scroll(function() {
    var iframes = $(".kw-iframe-lazy");
    iframes.each(function(index, el) {
      var src = $(el).data("src");
      $(el).attr("src", src);
      $(el).removeClass("kw-iframe-lazy");
    });
  });
});
("use strict");

(function() {
  (function(window, document) {
    "use strict";

    var file = "/assets/sprites/svg/symbol_sprite.html",
      revision = "fohreeghoo6Waith";
    if (
      !document.createElementNS ||
      !document.createElementNS("http://www.w3.org/2000/svg", "svg")
        .createSVGRect
    )
      return true;

    var isLocalStorage =
        "localStorage" in window && window["localStorage"] !== null,
      request,
      data,
      insertIT = function insertIT() {
        document.body.insertAdjacentHTML("afterbegin", data);
      },
      insert = function insert() {
        if (document.body) insertIT();
        else document.addEventListener("DOMContentLoaded", insertIT);
      };

    if (isLocalStorage && localStorage.getItem("inlineSVGrev") == revision) {
      data = localStorage.getItem("inlineSVGdata");

      if (data) {
        insert();
        return true;
      }
    }

    try {
      request = new XMLHttpRequest();
      request.open("GET", file, true);

      request.onload = function() {
        if (request.status >= 200 && request.status < 400) {
          data = request.responseText;
          insert();

          if (isLocalStorage) {
            localStorage.setItem("inlineSVGdata", data);
            localStorage.setItem("inlineSVGrev", revision);
          }
        }
      };

      request.send();
    } catch (e) {}
  })(window, document);
})();
("use strict");

$(document).ready(function() {
  var index = 0;
  $(".s9_slider_links").on("setPosition", function(event, slick) {
    $(".s9_slider_links .slick-list")[0].style.height = "auto";
    $(".s9_slider_links .slick-track")[0].style.height = "auto";
    $(".s9_slider_links .slick-track")[0].style.transform = "none";
  });
  $(".s9_slider_links").on("init", function(event, slick) {
    try {
      index = $(".s9_slider .slider-active")
        .closest(".slick-slide")
        .data("slick-index");

      if (index) {
        setTimeout(function() {
          slick.slickGoTo(index);
        }, 0);
      }
    } catch (err) {
      console.log(err);
    }
  });
  $(".s9_slider").slick({
    asNavFor: ".s9_slider_links",
    lazyLoad: "progressive",
    centerMode: true,
    centerPadding: "60px",
    slidesToShow: 1,
    arrows: false,
    vertical: true,
    verticalSwiping: true
  });
  $(".s9_slider_links").slick({
    asNavFor: ".s9_slider",
    centerMode: false,
    slidesToShow: 10,
    slidesToScroll: 1,
    arrows: false,
    vertical: true,
    verticalSwiping: true,
    infinite: false,
    focusOnSelect: true
  });
});
("use strict");

(function() {
  document.getElementById("copy_year").textContent = new Date().getFullYear();
})();
("use strict");

(function() {
  var $root = $("html, body");
  $('a[href^="#"]').click(function() {
    $root.animate(
      {
        scrollTop: $($.attr(this, "href")).offset().top
      },
      500
    );
    return false;
  });
})();

$(function() {
  $(document).scroll(function() {
    var $nav = $(".kw-navbar");
    $nav.toggleClass("scrolled", $(this).scrollTop() > $nav.height());
  });
});
